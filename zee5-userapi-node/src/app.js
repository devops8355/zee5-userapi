require('dotenv').config({ path:`./.env.${(process.env.NODE_ENV).trim()}`});
// require('dotenv').config({ path:`./.env.${(process.env.NODE_ENV)}`});

const express = require('express');
const cookieParser = require('cookie-parser');
const app = express();

// Middleware Setup
const errorMiddleware = require('../src/middlewares/appErrorHandler');
const mogran = require('../src/middlewares/morgan')
const util = require('./middlewares/utility')

//Model importing
require('./models/settings');
require('./models/watchlist');
require('./models/reminders');
require('./models/user');
require('./models/otpRepository');
require('./models/helthCheck');
require('./models/favorites');
require('./models/userProfileUpdateHistory');

//Express Methods
app.use(express.text());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(mogran);
app.use(express.static('./public'));

//import routes
// v1
const favoritesRouter = require("./routes/favorites");
const remindersRouter = require("./routes/reminders");
const settingRouter = require('./routes/settings');
const watchlistRouter = require('./routes/watchlist');
const userV1Router = require('./routes/user');
const wrapperRouter = require('./routes/wrapper');
const healthCheck = require('./routes/healthcheck');
const wrapperSettingsRouter = require('./routes/wrapperSettings');
const filesRouter = require('./routes/files');

// v2
const v2Router = require('./routes/v2');
const userV2Router = require('./routes/v2/user');



//CORS
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//Declaring Routes
// v1
app.use(`/v1/favorites`, favoritesRouter);
app.use(`/v1/reminders`, remindersRouter);
// app.use(`/v1/settings`, settingRouter);
app.use(`/v1/settings`, wrapperSettingsRouter);
app.use(`/v1/watchlist`, watchlistRouter);
app.use(`/v1/userReal`, userV1Router);
app.use(`/v1`, wrapperRouter);
app.use(`/v1/files`, filesRouter);

// app.use(`/v1`, healthCheck);

// v2
app.use(`/v2/user`, v2Router);
app.use(`/v2/userReal`, userV2Router);

//error handler miidleware
app.use(errorMiddleware.globalErrorHandler);
app.use(errorMiddleware.globalNotFoundHandler);
//mssql db connection

const sql = require('mssql')
const sqlConfig = require('./connections/connection');
sql.connect(sqlConfig).then(pool => {
  util.log('database connection open success','info')
}).catch(err => {
  util.log('MSSQL database connection error fix it and restart server','error',err)
});

//sequlize db connection
/*const sequlize = require('./connections/seqlizeConnect');
sequlize.sync()
  .then(cart => {
    console.log("database connection open success");
  }).catch(err => {
    console.log(err);
});*/

//Starting the Server
app.listen(process.env.PORT, () => console.debug(`App started on port ${process.env.PORT}`)).timeout = 600000;
module.exports = app;
