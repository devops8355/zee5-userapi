const Sequelize = require('sequelize');
const sequelize = new Sequelize("tempdb", "root", 
"root", {
  host: "localhost",
  port: 1433,  // <----------------The port number you copied
  dialect: "mssql",
  // operatorsAliases: false,
  dialectOptions: {
    options: {
        encrypt: false,
    }
  },
  // pool: {
  // max: 5,
  // min: 0,
  // acquire: 30000,
  // idle: 10000
  // }
});
module.exports = sequelize;