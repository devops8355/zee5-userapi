
/* response generation library for api
errCode = for errocodes if have
message = for global message
status = for api response status
data: response data
*/


module.exports={
  generate :(errCode=400, message='ERROR', status=400, data={}) => {
    let response = {
      error_code: errCode,
      error_msg: message,
      http_code: status,
      metaDetails: data
    }
    return response
  },
  generate400 : (outputCode, err,outputField,outputValue,outputMessage)=>{
    let response = {
        code: outputCode,
        message: err,
        fields: [
          {
            field: outputField,
            value: outputValue,
            message: outputMessage
          }
        ]
    }
    return response;
  },
    generate401 : (outputCode , outputMessage) =>{
      let response = {
        code : outputCode,
        message : outputMessage
      }
      return response;
    },
    
  
} 

