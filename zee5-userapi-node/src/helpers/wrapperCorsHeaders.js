
exports.setCorsHeaders = (req, res, next) => {
    let reqHeadersMethods = ["GET", "POST", "PUT", "UPDATE", "DELETE", "OPTIONS", "PATCH"];
    let reqAllowHeaders = [
        "Content-Type",
        "authorization",
        "Authorization",
        "X-Authorization",
        "Accept",
        "Origin",
        "User-Agent",
        "Cache-Control",
        "X-Requested-With",
        "X-ACCESS-TOKEN",
        "crossdomain",
        "x-access-token",
        "X-Z5-Appversion",
        "x-z5-appversion",
        "guest_token",
        "X-User-Type",
        "x-user-type",
        "X-Z5-AppPlatform",
        "x-z5-appplatform",
        "X-Z5-Guest-Token",
        "x-z5-guest-token"
    ];
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods",reqHeadersMethods.join(","));
    res.header("Access-Control-Allow-Headers",reqAllowHeaders.join(","));
    res.header("Access-Control-Allow-Credentials",true);
    next();

}