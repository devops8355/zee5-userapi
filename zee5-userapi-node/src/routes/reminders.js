const express = require('express');
const router = express.Router();
const remindersController = require('../controllers/remindersController');
const { verifyReminders } = require('../services/reminders/remindersValidation');

// get All reminders
router.get('/', remindersController.getReminders);

// add reminders
router.post('/', verifyReminders, remindersController.addReminders);

// update reminders
router.put('/', verifyReminders, remindersController.updateReminders);

// update reminders
router.delete('/', remindersController.deleteReminders);

//global reminders
module.exports = router;