const db = require('../connections/awsConnection');
const logger = require('../helpers/logger')
const config = require("../config/config");
const params1 = {
    TableName: config.reminders.table,
    AttributeDefinitions: [{
        AttributeName: 'Id', //primary key
        AttributeType: 'S'
    },
        {
            AttributeName: 'userId', // index key / sort key
            AttributeType: 'S'
        }
    ],
    KeySchema: [{
        AttributeName: 'Id',
        KeyType: 'HASH'
    },
        {
            AttributeName: 'userId',
            KeyType: 'RANGE'
        }
    ],
    GlobalSecondaryIndexes: [ // optional (list of GlobalSecondaryIndex)
        {
            IndexName: config.reminders.index,
            KeySchema: [{ // Required HASH type attribute
                AttributeName: 'userId',
                KeyType: 'HASH',
            }],
            Projection: { // attributes to project into the index
                ProjectionType: 'ALL' // (ALL | KEYS_ONLY | INCLUDE)
            },
            ProvisionedThroughput: { // throughput to provision to the index
                ReadCapacityUnits: 10,
                WriteCapacityUnits: 10,
            },
        },

    ],

    ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10
    },
};

db.dynamoDB.listTables({}, function(err, data) {
    let tables = null;
    if(data){
        tables= data.TableNames;
    }
    if(!data || tables.indexOf(params1.TableName) == -1){
        db.dynamoDB.createTable(params1, function (err, data) {
            if (err) {
                logger.error("Error: " + JSON.stringify(err))
            } else {
                logger.info("Table created: " + JSON.stringify(data))
            }
        });
    }
});