const response = require('./../helpers/responseHelper')
const logger = require('../helpers/logger')
const utill = require('../middlewares/utility');

let errorHandler = (error,req, res, next) => {
    if((process.env.NODE_ENV).trim()=='dev'){
        utill.log(error.message,'error',error);
    }
    let responseError={};
    error.status = error.status || 400;
    responseError.code = error.statusCode || 0;
    responseError.message = error.message || 'ERROR';
    if(error.data){
        responseError.fields = error.data;
    }
   return res.status(error.status).send(responseError);
    
}// end request ip logger function  

let notFoundHandler = (req,res,next)=>{
    const status = 404;
    const message = "Not Found";
    // const data = error.data || null;
    let apiResponse = response.generate(status, message,status,null)
    return res.status(status).send(apiResponse)

}// end not found handler

module.exports = {
    globalErrorHandler : errorHandler,
    globalNotFoundHandler : notFoundHandler
}
