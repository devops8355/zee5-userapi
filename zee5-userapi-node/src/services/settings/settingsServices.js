const sql = require('mssql');
const db = require('../../connections/awsConnection')
const { v4: uuidv4 } = require('uuid');
const dynamoDB = require('../../connections/dynamoConnection');
const logger = require('../../helpers/logger');
const config = require('../../config/config');
// const TableName = 'Settings';
// const IndexName = 'user_index'
const TableName = config.settings.table;
const IndexName = config.settings.index;

exports.getSettingsData = async(UserId)=>{
  return dynamoDB.getDynamo({
    TableName: TableName,
    IndexName: IndexName,
    ProjectionExpression:'Id,SettingKey,SettingValue',
    KeyConditionExpression: 'UserId = :UserId',
    ExpressionAttributeValues: { 
      ":UserId": UserId
    },
  });
}

exports.addSettingsData = async (req,res) => {
  let UserId=req.headers.userid;
  let body = req.body;
  const isKeyExist =await dynamoDB.getDynamo({
    TableName: TableName,
    IndexName: IndexName,
    KeyConditionExpression: 'UserId = :UserId',
    FilterExpression: 'SettingKey = :SettingKey',
    ExpressionAttributeValues: { 
      ':UserId': UserId,
     ':SettingKey': body.key
    }
  });

  if(isKeyExist.Items.length > 0){
    const error = new Error();
    error.message = 'Item already exists'
    error.statusCode = 2511;
    error.status = 400;
    logger.error(JSON.stringify(error));
    throw error;
  }
  let addItemParam ={
    TableName: TableName,
    Item: {
      Id:uuidv4(),
      UserId: UserId,
      SettingKey:body.key,
      SettingValue:body.value,
      LastUpdate: Date.now(),
      LastMigration:Date.now()
    }
  }
  const insertValue =await dynamoDB.addDynamo(addItemParam); 
   return addItemParam.Item;
} 



exports.updateSettingsData = async (req,res) => {
  let userId=req.headers.userid;
  let body = req.body;
  const isKeyExist =await dynamoDB.getDynamo({
    TableName: TableName,
    IndexName: IndexName,
    KeyConditionExpression: 'UserId = :UserId',
    FilterExpression: 'SettingKey = :SettingKey',
    ExpressionAttributeValues: { 
      ':UserId': userId,
      ':SettingKey': body.key
    }
  }); 
  if(isKeyExist.Items.length == 0){
    const error = new Error();
    error.message = "Item couldn't be found"
    error.statusCode = 2;
    error.status = 404;
    logger.error(JSON.stringify(error));
    throw error;
  }

  const params = {
    TableName: TableName,
    Key: {
        "UserId": userId,
        "Id":isKeyExist['Items'][0].Id
    },
    UpdateExpression: "SET SettingValue = :SettingValue, LastUpdate = :LastUpdate",
    ExpressionAttributeValues: {
        ":SettingValue":  body.value,
        ":LastUpdate":Date.now()
    },
    ReturnValues: "UPDATED_NEW",
  };
  const insertValue =await dynamoDB.updateDynamo(params); 
  return {Id:isKeyExist['Items'][0].Id,UserId:userId,SettingValue:body.value};
}

exports.deleteSettingsData = async (req,res) => {
  let userId=req.headers.userid;
  let body = req.query;
  const isKeyExist =await dynamoDB.getDynamo({
    TableName: TableName,
    IndexName: IndexName,
    KeyConditionExpression: 'UserId = :UserId',
    FilterExpression: 'SettingKey = :SettingKey',
    ExpressionAttributeValues: { 
      ':UserId': userId,
      ':SettingKey': body.key
    }
  }); 

  if(isKeyExist.Items.length == 0){
    const error = new Error();
    error.message = "Item couldn't be found"
    error.statusCode = 2;
    error.status = 404;
    logger.error(JSON.stringify(error));
    throw error;
  }
  const isDeleted = await dynamoDB.deleteDynamo({
    TableName: TableName,
    Key:{
      "Id": isKeyExist['Items'][0].Id,
      "UserId": userId
    }
  }); 
  return  {Id:isKeyExist['Items'][0].Id,UserId:userId};
}
