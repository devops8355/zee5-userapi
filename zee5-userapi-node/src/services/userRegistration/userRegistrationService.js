
const axios = require('axios');
const dns = require('dns');
const crypto = require('crypto');
const utf8 = require('utf8');
const redisCluster = require('../../connections/redisConnection');

const COUNTRY_API_URL = process.env.COUNTRY_API_URL;
const B2BURL = 'https://stagingb2bapi.zee5.com/partner/api/silentregister.php'
const USER_API_URL = process.env.USER_API_BASE_URL+'/';

const USERAPI_SIGNINGKEY = process.env.USERAPI_SIGNINGKEY;
const USERAPI_USERSECRET = process.env.USERAPI_USERSECRET;
const logger = require('../../helpers/logger')


// get user country code
exports.getRegistrationCountryCode = (ip) => {
  // Make a request to find User country
  let country_code = 'IN';
  return axios.get(COUNTRY_API_URL + ip)
    .then(function (response) {
      // handle success
      logger.info("response======"+JSON.stringify(response.data))
      if (response.data.status) {
        return response.data.country_code ? response.data.country_code : country_code;
      } else {
        return country_code;
      }

    })
    .catch(function (error) {
      // handle error
      logger.error(JSON.stringify(error));
      return country_code;

    });


}

exports.sendOTP = async (mobileno, otp, version = null) => {
  /*send this otp on user mobile number*/
  let result = {
    status: false,
    data: null
  }
  try {

    let msg = "OTP:" + otp + " - Zee5"
    return axios.get("https://b2bapi.zee5.com/sendsms.php?mobileno=" + mobileno + "&messagetext=" + msg)
      .then(function (res) {
        // handle success

        if (res) {
          result.status = true;
          if (version && version == 2) {
            result.data = { "code": 1 };

          } else {
            result.data = { "code": 1, "message": "Notification has been sent to queue" };
          }
          return result.data;
        } else {
          result.status = false;
          result.data = { "code": "404", "message": "Message can not be sent" };
          return result.data;
        }

      })
      .catch(function (error) {
        // handle error
        logger.error(JSON.stringify(error));
        result.status = false;
        result.data = { "code": "404", "message": "Message can not be sent" };
        return result.data;

      });

  } catch (err) {
    logger.error(JSON.stringify(err));
    result.status = false;
    result.data = { "code": "404", "message": "Message can not be sent" };
    return result.data;
  }

}

exports.checkPasswordOTPInRedis = async (key, otp) => {
  let responseFromRedis = await redisCluster.get(key);
  logger.info(JSON.stringify(responseFromRedis)+otp)
  if (responseFromRedis == otp) {
    otp = this.rand(1000, 9999);
    logger.info(otp + "password in redis");
    return checkPasswordOTPInRedis(key, otp);
  }
  return otp;
}

exports.createTempRecordInRedis = (key, hashField, hashValue) => {
  /*Create key in Redis*/
  redisCluster.hset(key, hashField, hashValue);
  redisCluster.expire(key, 900); //set TTL of 15mins
}

exports.createPasswordRecordInRedis = (key, value) => {
  /*Create key in Redis*/
  redisCluster.set(key, value);
  redisCluster.expire(key, 1800) //set TTL of 30mins
}

exports.rand = (min, max) => {

  let mini = Math.ceil(min);
  let maxi = Math.floor(max);
  logger.info(Math.floor(Math.random() * (maxi - mini) + mini))
  return Math.floor(Math.random() * (maxi - mini) + mini); //The maximum is exclusive and the minimum is inclusive

}

exports.getIpofUser = (host) => {
  dns.lookup(host, (err, addresses, family) => {
    // Print the address found of user
    return addresses;
    // Print the family found of user
  });

}
let createAuthKeys = async (body, path) => {

  // let key =await sm.getkeys("USERAPI_SIGNINGKEY");
  // const finalKey = utf8.encode(key);
  const finalKey = await utf8.encode(USERAPI_SIGNINGKEY); // utf8_encode(USERAPI_SIGNINGKEY);
  let data = body + ":" + path;
  let finalData = utf8.encode(data);  // utf8_encode($data);
  let hashedData = crypto.createHmac('sha256', finalKey).update(finalData)   // hash_hmac("sha256", $finalData, $finalKey, true); 
  let base64EncodedData = hashedData.digest('base64') // base64_encode($hashedData);
  // return "Z5 "+ sm.getkeys(USERAPI_USERSECRET)+":"+base64EncodedData ;
  return "Z5 " + USERAPI_USERSECRET + ":" + base64EncodedData;

}

exports.checkuserExist = async (mobile_number) => {

  if (!mobile_number) {
    return { "code": "404", "message": "Please provide mobile number" }
  }
  let mobilenumber = mobile_number ? mobile_number : "";
  let query = "mobile=" + mobilenumber;
  let type = "mobile";
  let body = { mobile: mobile_number };
  let path = "/v1/manage/customer/token?" + query;
  body = '';
  let result = {
    status: false,
    data: null
  }
  let authkey = await createAuthKeys(body, path);
  const options = {
    headers: {
      "Accept": "application/json",
      "Authorization": authkey,
      "Content-Type": "application/json-patch+json"
    }
  };

  return axios.get(USER_API_URL + path, options)
    .then((res) => {
      if (res.response.status === 200) {
        result.status = true;
        result.statusCode = 400;
        result.data = { "code": 0, "message": "User with same mobile already exists" };
        return result;
      } else if (res.response.status === 404) {
        result.status = false;
        result.statusCode = 404;
        return result;
      }
    }, (error) => {
      logger.error("Request failed with status code 401")
      logger.error(JSON.stringify(error))
      return result;
    });


}



exports.loginwithB2B = (userdata) => {
  return axios.post(B2BURL, userdata)
    .then((res) => {
      return res;
    }, (error) => {
      logger.error(JSON.stringify(error));
      return error;
    });
}

