
var fs = require('fs');
const awsObj = require("../../connections/awsConnection");
const logger = require('../../helpers/logger');
const uploadFile = require("../../middlewares/upload");

exports.uploaDumpFile = async (req, res, next) => {

  try {
    await uploadFile(req, res);
    if (req.file == undefined) {
      res.send({ message: "No File Selected!" });
    }
    else {
      logger.info(`${req.file.filename} File Uploaded!`);
      return req.file.path;
    }
  } catch (error) {
    if (!err.statusCode) {
      err.message = "File couldn't be uploaded!";
      err.statusCode = 500;
    
    next(err);
  }
} 
};

 exports.seedData = async (req,res, next) => {

  const error = new Error();
   if(!req.body.path || !req.body.table_name){
  
    error.message = "File path OR table name not provided"
    error.statusCode = 2;
    error.status = 404;
    logger.error(JSON.stringify(error));
    throw error;
   }
   else if (!fs.existsSync(req.body.path)){

    error.message = "File doesn't exist"
    error.statusCode = 2;
    error.status = 404;
    logger.error(JSON.stringify(error));
    throw error;
   }

  let jsonData = JSON.parse(fs.readFileSync(req.body.path, 'utf8'));
  let params = { TableName : req.body.table_name } 

  getRecordsCount(params, "Before Seeding");
  logger.info("Importing Data into DynamoDB. Please wait.");

  jsonData.forEach(function(record) {
  
      try {
              var param = {
                  TableName: params.TableName,
                  Item:  record
              };      

          awsObj.dynamo.put(param, function(err, data) {
              if (err) {
                  logger.error("Unable to add record record ID", record.Id, " Error:", JSON.stringify(err, null, 2))
              } else {
                  console.log("Put Item succeeded record ID:", record.Id);
              }
           });
                
      } catch (error) {
        logger.error(JSON.stringify(error))
        if (!err.statusCode) {
          error.message = "Item couldn't be added (DB insert failure)";
          error.statusCode = 500;
        }
        next(err);
      }
  });

   result = await getRecordsCount(params, "After Seeding");
   return result;
 }


 // get records count in Dynamo DB table
 async function getRecordsCount(params, callAt) {
   return  await awsObj.dynamo.scan(params).promise()
    .then((data) => {
      logger.info(`total records ${callAt} : `, data.Count); 
      return data.Count;
    })
    
    
}
