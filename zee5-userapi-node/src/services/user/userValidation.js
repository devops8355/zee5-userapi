const requestObject = require('../../helpers/requestHelper');
const {header, validationResult} = require("express-validator");
const logger = require('../../helpers/logger')

module.exports = {

    verifyConfirmationKey : (req, res, next) => {
        let error = new Error();
        let body = req.body;
        let code = req.body.hasOwnProperty('code') ? req.body.code : req.query.code;
        let userid = req.headers.userid;

        // Validate request object and its fields
        if (requestObject.isEmpty(body) || isNullOrEmpty(code) || (isNullOrEmpty(userid))) {

            error = {
                message: 'Invalid input Parameter',
                statusCode: 3,
                status: 400,
                fields: [{field: "code", message: "No confirmation code in request"}]
            }
            logger.error(JSON.stringify(error))

        } else {
            next();
        }
    },

    getUser: getUser = [
        header('userid').exists(),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {

                let error = new Error();
                error = {message: "Please provide a user id", statusCode: 2, status: 422}
                logger.error(JSON.stringify(error));

            } else {
                next();
            }
        },
    ],
    getUserWithEmail: (req,res,next) =>{
        let error = new Error();
        let body = req.body;
        let email = req.body.hasOwnProperty('email') ? req.body.email : req.query.email;
        let password = req.body.hasOwnProperty('password') ? req.body.password : req.query.password;
        
        let userid = req.headers.userid;

        // Validate request object and its fields
        if (requestObject.isEmpty(body) || isNullOrEmpty(email) || (isNullOrEmpty(password))) {

            error = {
                message: 'Invalid input Parameter',
                statusCode: 3,
                status: 400,
                fields: [{field: "code", message: "No confirmation code in request"}]
            }
            logger.error(JSON.stringify(error))

        } else {
            next();
        }
    },
    changePasswordValidator : (req,res,next) =>{
        let error = new Error();
        let body = req.body;
        let oldPassword = req.body.hasOwnProperty('old_password') ? req.body.old_password : req.query.old_password;
        let newPassword = req.body.hasOwnProperty('new_password') ? req.body.new_password : req.query.new_password;
        
        if(requestObject.isEmpty(body) || !(req.body.hasOwnProperty('old_password')) || !(req.body.hasOwnProperty('new_password')) ) {
            error = {
                code: 3,
                message: 'Invalid Json provided'
            }
            logger.error(JSON.stringify(error))
            return res.status(400).send(error);
        }

        if (oldPassword === '' && newPassword === '') {
            error = {
                "code": 3,
                "message": "Invalid input parameter",
                "fields": [
					{
					"field": "NewPassword",
					"message": "The NewPassword field is required."
					},
					{
					"field": "OldPassword",
					"message": "The OldPassword field is required."
					}
                ]
            }
			logger.error(JSON.stringify(error))
            return res.status(400).send(error);
        }

        // Validate request object and its fields
        if (oldPassword === '') {
            error = {
                "code": 3,
                "message": "Invalid input parameter",
                "fields": [{
                    "field": "OldPassword",
                    "message": "The OldPassword field is required."
                }]
            }
            logger.error(JSON.stringify(error))
            return res.status(400).send(error);
        }

        if(isNullOrWhitespace(oldPassword) || oldPassword.trim().length < 6 ){
            error = {
                "code": 3,
                "message": "Invalid input parameter",
                "fields": [{
                    "field": "OldPassword",
                    "message": "The field OldPassword must be a string with a minimum length of 6 and a maximum length of 2147483647."
                }]
            }
            logger.error(JSON.stringify(error))
            return res.status(400).send(error);
        }

        if (newPassword === '') {
            error = {
                "code": 3,
                "message": "Invalid input parameter",
                "fields": [{
                    "field": "NewPassword",
                    "message": "The NewPassword field is required."
                }]
            }
            logger.error(JSON.stringify(error))
            return res.status(400).send(error);
        }

        if(isNullOrWhitespace(newPassword) || newPassword.trim().length < 6 ){
            error = {
                "code": 3,
                "message": "Invalid input parameter",
                "fields": [
                  {
                    "field": "newPassword",
                    "message": "The field newPassword must be a string with a minimum length of 6 and a maximum length of 2147483647."
                  }
                ]
            }
            logger.error(JSON.stringify(error))
            return res.status(400).send(error);
        }

        if (Object.keys(error).length) {
            logger.error(JSON.stringify(error))
            return res.status(400).send(error);
        } else {
            next();
        }
    },
}
function isNullOrEmpty(input){
    return(input == null || input ==="");
}

function isNullOrWhitespace(input ) {
    if ( typeof input === 'undefined' || input === null ||  /\s/g.test(input) ) return true;
    return false;
}
