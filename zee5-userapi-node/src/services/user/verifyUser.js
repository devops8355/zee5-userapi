const dynamoDB = require('../../connections/dynamoConnection');
const config = require('../../config/config');
const usertableName = config.users.table;


module.exports ={
    validateUser : async (req) => {
    
        try {
        const getParams = {
            TableName: usertableName,
            KeyConditionExpression: 'Id = :id',
            ExpressionAttributeValues: {
                ':id':req.headers.userid
           }
       };
            data = await dynamoDB.getDynamo(getParams);
            return data;    
          } catch (error) {
            console.error(error);
       }  
   },
}