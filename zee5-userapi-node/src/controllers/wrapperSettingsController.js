const axiosHelper = require('../../src/helpers/axiosCalls')

const SETTINGS_URL = process.env.USER_API_BASE_URL+'/v1/settings'
module.exports = {
    getSettings: async (req, res, next) => {
        try {
          const headers = req.headers;
          authorization = headers['authorization']; 
          result =  await axiosHelper.call('GET', SETTINGS_URL, '',{
                "Accept":"application/json",
                "Authorization": authorization
            }
          );
          if (result.status === 200) {
            res.status(200).send(result.data);
          } else {
            return res.status(401).send({
              "code": 401,
              "message": "Authorization failed",
            })
          }
        } 
        catch(error) {
          if (error.hasOwnProperty("response") && error.response.hasOwnProperty("data")) {
            res.status(error.response.status).send(error.response.data);
          } else {
            res.status(404).send({ code: "1", message: error.message });
          }
        }
      },
      addSettings: async (req, res, next) => {
        try {
          const bodyData = req.body;
          const headers = req.headers;
          authorization = headers['authorization'];
          result = await axiosHelper.call('POST', SETTINGS_URL, bodyData, {
              'Content-Type': 'application/json',
              'Authorization': authorization,
            }
          );
          if (result.status === 200) {
            res.status(200).send(result.data);
          } else {
            return res.status(401).send({
              "code": 401,
              "message": "Authorization failed",
            })
          }
        } 
        catch(error) {
          if (error.hasOwnProperty("response") && error.response.hasOwnProperty("data")) {
            res.status(error.response.status).send(error.response.data);
          } else {
            res.status(404).send({ code: "1", message: error.message });
          }
        }
      },
    
      updateSettings: async (req, res, next) => {
        try {
          const bodyData = req.body;
          const headers = req.headers;
          authorization = headers['authorization'];
          result = await axiosHelper.call('PUT', SETTINGS_URL, bodyData, {
              'Content-Type': 'application/json',
              'Authorization': authorization,
            }
          );
          if (result.status === 200) {
            res.status(200).send(result.data);
          } else {
            return res.status(401).send({
              "code": 401,
              "message": "Authorization failed",
            })
          }
        } 
        catch(error) {
          if (error.hasOwnProperty("response") && error.response.hasOwnProperty("data")) {
            res.status(error.response.status).send(error.response.data);
          } else {
            res.status(404).send({ code: "1", message: error.message });
          }
        }
      },
    
      deleteSettings: async (req, res, next) => {
        try {
          const headers = req.headers;
          authorization = headers['authorization'];
          result = await axiosHelper.call('DELETE', SETTINGS_URL+'?key='+req.query["key"], '', {
              'Content-Type': 'application/json',
              'Authorization': authorization,
            }
          );
          if (result.status === 200) {
            res.status(200).send(result.data);
          } else {
            return res.status(401).send({
              "code": 401,
              "message": "Authorization failed",
            })
          }
        } 
        catch(error) {
          if (error.hasOwnProperty("response") && error.response.hasOwnProperty("data")) {
            res.status(error.response.status).send(error.response.data);
          } else {
            res.status(404).send({ code: "1", message: error.message });
          }
        }
    },
}